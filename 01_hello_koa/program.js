const koa = require('koa');
const app = new koa();
const port = process.argv[2];

// handlers here
app.use(async (ctx) => {
    ctx.body = "hello koa";
});

app.listen(port);