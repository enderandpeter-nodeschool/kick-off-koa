const koa = require('koa');
const app = new koa();
const port = process.argv[2];

// handlers here
app.use(async (ctx, next) => {
    switch(ctx.path){
        case '/':
            ctx.body = 'hello koa'
        break;
        case '/404':
            ctx.status = 404
            ctx.body = 'page not found'
        break;
        case '/500':
            ctx.status = 500
            ctx.body = 'internal server error'
        break;
        default:
            next()
    }
});

app.listen(port);