const koa = require('koa');
const app = new koa();
const parse = require('co-body')
const port = process.argv[2];

// handlers here
app.use(async (ctx, next) => {
    const body = await parse(ctx.req)
    if(ctx.method === 'POST' && body.name){
        ctx.body = body.name.toUpperCase()
    } else {
        next()
    }
});

app.listen(port);