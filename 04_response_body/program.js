const koa = require('koa')
const app = new koa()
const fs = require('fs')
const port = process.argv[2]
const file = process.argv[3]

// handlers here
app.use(async (ctx, next) => {
    switch(ctx.path){
        case '/json':
            ctx.body = {foo: 'bar'}
        break
        case '/stream':
            ctx.body = fs.createReadStream(file)
        break
        default:
            next()
    }
});

app.listen(port);