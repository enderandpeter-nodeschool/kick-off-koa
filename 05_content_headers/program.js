const koa = require('koa')
const app = new koa()
const port = process.argv[2]

// handlers here
app.use(async (ctx, next) => {
    if(ctx.is('application/json')){
        ctx.body = {message: 'hi!'}
    } else {
        ctx.body = 'ok'
    }
});



app.listen(port);