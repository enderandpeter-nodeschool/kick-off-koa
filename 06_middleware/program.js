const koa = require('koa')
const app = new koa()
const port = process.argv[2]

app.use(async (ctx, next) => {
    const start = Date.now()
    await next()
    const ms = Date.now() - start
    ctx.set('X-Response-Time', `${ms}ms`)
});

app.use(async (ctx, next) => {
    await next()
    ctx.body = ctx.body.toUpperCase()
});

app.use(async (ctx, next) => {
    ctx.body = 'hello koa'
});

app.listen(port);