const koa = require('koa')
const app = new koa()
const port = process.argv[2]

app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      ctx.status = 500
      ctx.body = 'internal server error'
    }
  })

app.use(async (ctx, next) => {
    if (ctx.path === '/error') throw new Error('ooops');
    ctx.body = 'OK';
})

app.listen(port)