const koa = require('koa')
const app = new koa()
const port = process.argv[2]

app.keys = [ 'secret', 'keys' ]

app.use(async (ctx, next) => {
    if(ctx.path === '/'){
        let viewCount = ctx.cookies.get('view', {signed: true})
        if(!viewCount){
            viewCount = 1
            ctx.cookies.set('view', 1, {signed: true})
        } else {
            viewCount = Number.parseInt(viewCount)
            viewCount++
            ctx.cookies.set('view', viewCount, {signed: true})
        }

        ctx.body = `${viewCount} views`
    }
})

app.listen(port)