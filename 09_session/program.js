const session = require('koa-session');
const koa = require('koa')
const app = new koa()
const port = process.argv[2]

app.keys = [ 'secret', 'keys' ]

app.use(session({signed: true}, app))

app.use(async (ctx, next) => {
    if(ctx.path === '/'){
        let viewCount = ctx.cookies.get('view')
        if(!viewCount){
            viewCount = 1
            ctx.cookies.set('view', 1)
        } else {
            viewCount = Number.parseInt(viewCount)
            viewCount++
            ctx.cookies.set('view', viewCount)
        }

        ctx.body = `${viewCount} views`
    }
})

app.listen(port)