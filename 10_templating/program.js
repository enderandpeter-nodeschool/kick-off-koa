const koa = require('koa')
const app = new koa()
const views = require('co-views');
const port = process.argv[2]

const render = views(__dirname + '/views', {
    ext: 'ejs'
  });

app.use(async (ctx, next) => {
    if(ctx.method === 'GET' && ctx.path === '/'){
        const user = {
            name: {
              first: 'Tobi',
              last: 'Holowaychuk'
            },
            species: 'ferret',
            age: 3
          };
        
        ctx.body = await render('user', {user});
    }
})

app.listen(port)