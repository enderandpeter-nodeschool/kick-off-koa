const koa = require('koa');
const parse = require('co-body');
const session = require('koa-session');

const form = '<form action="/login" method="POST">\
    <input name="username" type="text" value="username">\
    <input name="password" type="password" placeholder="The password is \'password\'">\
    <button type="submit">Submit</button>\
</form>';

const app = new koa();

// use koa-session somewhere at the top of the app
// we need to set the `.keys` for signed cookies and the cookie-session module
app.keys = ['secret1', 'secret2', 'secret3'];
app.use(session({signed: true}, app))

/**
 * If `this.session.authenticated` exist, user will see 'hello world'
 * otherwise, people will get a `401` error  because they aren't logged in
 */

app.use(async (ctx, next) => {
    if (ctx.path !== '/') return next()

    if(ctx.cookies.get('authenticated')){
        ctx.body = 'hello world'
    } else {
        ctx.status = 401
    }
});

/**
 * If successful, the logged in user should be redirected to `/`.
 * hint: use `this.redirect`
 */

app.use(async (ctx, next) => {
    
    if (ctx.path !== '/login') return next()

    const body = await parse(ctx.req)
    if (ctx.method === 'GET') {
        if(!ctx.cookies.get('authenticated')){
            ctx.body = form
        } else {
            ctx.redirect('/')
        }
    }

    if(ctx.method === 'POST'){
        if(body.username === 'username' && body.password === 'password'){
            ctx.cookies.set('authenticated', true)
            ctx.redirect('/')
        } else {
            ctx.status = 400
        }
    }
});

/**
 * Let's redirect to `/login` after every response.
 * If a user hits `/logout` when they're already logged out,
 * let's not consider that an error and rather a "success".
 */

app.use(async (ctx, next) => {
    if (ctx.path !== '/logout') return next();

    ctx.cookies.set('authenticated', false)
    ctx.redirect('/login')
});

app.listen(process.argv[2]);